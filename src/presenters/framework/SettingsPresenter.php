<?php

namespace Varhall\Framework\Presenters\Framework;

/**
 * Description of SettingsPresenter
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class SettingsPresenter extends \Varhall\Framework\Presenters\Core\ApiPresenter
{
    /**
     * @var \Varhall\Framework\Model\Services\Framework\Customers\CustomersService
     */
    protected $_customersService = NULL;
    
    /**
     * @var \Varhall\Framework\Model\Services\Framework\Users\UsersService
     */
    protected $_usersService = NULL;
   

    public function __construct(\Varhall\Framework\Model\Services\Framework\Customers\CustomersService $customersService, 
                                \Varhall\Framework\Model\Services\Framework\Users\UsersService $usersService)
    {
        parent::__construct();

        $this->_customersService = $customersService;
        $this->_usersService = $usersService;
    }
    
    public function renderRestList()
    {
        $customer = $this->currentCustomer()->toArray();
        $customer['current_quotas'] = [
            'users'     => $this->_usersService->findAll()->count()
        ];
        
        $this->sendJson($customer, [ 'domain', 'note' ]);
    }

    public function renderRestGet($id)
    {
        $this->renderRestList();
    }

    public function renderRestUpdate($id, array $data)
    {
        $data = $this->filterRequestData($data, ['invoice_name', 'invoice_street', 'invoice_city', 'invoice_zip', 'invoice_company_number', 'invoice_vat_number', 'options']);
        $this->validateRequestData($data, [
            'invoice_name'              => ['string:..100'],
            'invoice_street'            => ['string:..100'],
            'invoice_city'              => ['string:..100'],
            'invoice_zip'               => ['string:..100'],
            'invoice_company_number'    => ['string:..100'],
            'invoice_vat_number'        => ['string:..100'],
            'options'                   => ['array'],
        ]);

        $item = $this->_customersService->update($this->currentCustomer()->id, $data);
        
        $this->sendJson('ok');
    }
}
