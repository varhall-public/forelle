<?php

namespace Varhall\Framework\Presenters\Framework;

/**
 * Description of SettingsPresenter
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class ProfilesPresenter extends \Varhall\Framework\Presenters\Core\ApiPresenter
{
    /**
     * @var \Varhall\Framework\Model\Services\Framework\Users\UsersService
     */
    protected $_usersService = NULL;
    

    public function __construct(\Varhall\Framework\Model\Services\Framework\Users\UsersService$usersService)
    {
        parent::__construct();

        $this->_usersService = $usersService;
    }

    public function renderRestList()
    {
        $this->sendJson($this->_usersService->findById($this->user->id), [ 'field_exclude' => ['password']]);
    }

    public function renderRestGet($id)
    {
        $this->renderRestList();
    }

    public function renderRestCreate(array $data)
    {
        $this->sendJsonError('Method is unsupported', \Nette\Http\Response::S405_METHOD_NOT_ALLOWED);
    }
    
    public function renderRestUpdate($id, array $data)
    {    
        $old_password = isset($data['old_password']) ? $data['old_password'] : NULL;
        
        $data = $this->filterRequestData($data, ['email', 'password', 'name', 'surname', 'gender']);
        $this->validateRequestData($data, [
            'email'     => 'email',
            'password'  => 'string:6..',
            'gender'    => 'enum:male,female'
        ]);
        
        if (isset($data['password']) && !$this->_usersService->verifyPassword($this->user->id, $old_password))
            $this->sendJsonError('Invalid old password', \Nette\Http\Response::S400_BAD_REQUEST);

        $item = $this->_usersService->update($this->user->id, $data);
        
        $this->sendJson('ok');
    }
    
    public function renderRestDelete($id)
    {
        $this->sendJsonError('Method is unsupported', \Nette\Http\Response::S405_METHOD_NOT_ALLOWED);
    }
}
