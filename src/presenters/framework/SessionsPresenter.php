<?php

namespace Varhall\Framework\Presenters\Framework;

/**
 * API Presenter urceny pro prihlasovani
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class SessionsPresenter extends \Varhall\Framework\Presenters\Core\ApiPresenter
{
    public function renderRestList()
    {
        $this->sendJsonError('Unable to get session list', \Nette\Http\Response::S400_BAD_REQUEST);
    }
    
    public function renderRestGet($id)
    {
        if ($id != $this->user->id)
            $this->sendJsonError('You can only take session from your ID', \Nette\Http\Response::S403_FORBIDDEN);
        
        $this->sendJson([
            'session'   => NULL,
            'user'      => $this->user->getIdentity()->getData()
        ]);
    }
    
    public function renderRestCreate(array $data)
    {
        $data = $this->filterRequestData($data, ['username', 'password']);
        $this->validateRequestData($data, [
            'username'  => 'required',
            'password'  => 'required'
        ]);
        
        try {
            $expiration = '60 minutes';
            
            $time = \Nette\Utils\DateTime::from($expiration)->format('U');
            $expireTime = $time;
            $expireDelta = $time - time();
            
            $this->user->setExpiration($expiration);
            $this->user->login($data['username'], $data['password']);
            
            $this->sendJson([
                'session'   => [
                            'id'            => date('YmdHis') . rand(100000, 999999),
                            'expire_time'   => $expireTime,
                            'expire_delta'  => $expireDelta
                ],
                'user'      => $this->user->getIdentity()->getData()
            ]);
            
        } catch (\Nette\Security\AuthenticationException $ex) {
            $this->sendJsonError($ex->getMessage(), \Nette\Http\Response::S403_FORBIDDEN);
        }
    }
    
    public function renderRestDelete($id)
    {
        $this->user->logout(TRUE);
        $this->sendJson('ok');
    }
}
