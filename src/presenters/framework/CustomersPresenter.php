<?php

namespace Varhall\Framework\Presenters\Framework;

/**
 * Description of CustomersPresenter
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class CustomersPresenter extends \Varhall\Framework\Presenters\Core\ApiPresenter
{
    /**
     * @var \Varhall\Framework\Model\Services\Framework\Customers\CustomersService
     */
    protected $_customersService = NULL;

    public function __construct(\Varhall\Framework\Model\Services\Framework\Customers\CustomersService $customersService)
    {
        parent::__construct();

        $this->_customersService = $customersService;
    }

    public function renderRestList()
    {
        $this->sendJson($this->_customersService->findAll());
    }

    public function renderRestGet($id)
    {
        $this->sendJson($this->_customersService->findById($id));
    }

    public function renderRestCreate(array $data)
    {
        $data = $this->filterRequestData($data, ['name', 'note', 'invoice_name', 'invoice_street', 'invoice_city', 'invoice_zip', 'invoice_company_number', 'invoice_vat_number', 'payment_interval', 'paid_until', 'quotas', 'options', 'active']);
        $this->validateRequestData($data, [
            'name'                      => ['required', 'string:..100'],
            'note'                      => ['string'],
            'invoice_name'              => ['required', 'string:..100'],
            'invoice_street'            => ['required', 'string:..100'],
            'invoice_city'              => ['required', 'string:..100'],
            'invoice_zip'               => ['required', 'string:..100'],
            'invoice_company_number'    => ['required', 'string:..100'],
            'invoice_vat_number'        => ['required', 'string:..100'],
            'payment_interval'          => ['required', 'int:0..'],
            'paid_until'                => ['required', 'date'],
            'quotas'                    => ['required', 'int:0..'],
            'options'                   => ['required', 'array'],
            'active'                    => ['required', 'bool'],
        ]);

        $item = $this->_customersService->insert($data);
        $this->sendJson($item);
    }

    public function renderRestUpdate($id, array $data)
    {
        $data = $this->filterRequestData($data, ['name', 'note', 'invoice_name', 'invoice_street', 'invoice_city', 'invoice_zip', 'invoice_company_number', 'invoice_vat_number', 'payment_interval', 'paid_until', 'quotas', 'options', 'active']);
        $this->validateRequestData($data, [
            'name'                      => ['string:..100'],
            'note'                      => ['string'],
            'invoice_name'              => ['string:..100'],
            'invoice_street'            => ['string:..100'],
            'invoice_city'              => ['string:..100'],
            'invoice_zip'               => ['string:..100'],
            'invoice_company_number'    => ['string:..100'],
            'invoice_vat_number'        => ['string:..100'],
            'payment_interval'          => ['int:0..'],
            'paid_until'                => ['date'],
            'quotas'                    => ['int:0..'],
            'options'                   => ['array'],
            'active'                    => ['bool'],
        ]);
        
        $item = $this->_customersService->update($id, $data);
        $this->sendJson(NULL);
    }

    public function renderRestDelete($id)
    {
        $this->_customersService->delete($id);

        $this->sendJson('ok');
    }

}
