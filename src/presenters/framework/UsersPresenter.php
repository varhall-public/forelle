<?php

namespace Varhall\Framework\Presenters\Framework;

/**
 * API presenter urceny pro spravu uzivatelu
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class UsersPresenter extends \Varhall\Framework\Presenters\Core\ApiPresenter
{
    /**
     * @var \Varhall\Framework\Model\Services\Framework\Users\UsersService
     */
    protected $_usersService = NULL;
    
    public function __construct(\Varhall\Framework\Model\Services\Framework\Users\UsersService $usersService)
    {
        parent::__construct();
        
        $this->_usersService = $usersService;
    }

    public function renderRestList()
    {
        $this->sendJson($this->_usersService->findAll(), [ 'field_exclude' => ['password']]);
    }
    
    public function renderRestGet($id)
    {
        $this->sendJson($this->_usersService->findById($id), [ 'field_exclude' => ['password']]);
    }
    
    public function renderRestCreate(array $data)
    {
        $original = $data;

        $data = $this->filterRequestData($data, ['email', 'password', 'name', 'surname', 'active', 'role_id', 'gender']);
        $this->validateRequestData($data, [
            'email'     => ['required', 'email'],
            'password'  => ['required', 'string:6..'],
            'name'      => 'required',
            'surname'   => 'required',
            'role_id'   => ['required', 'int:1..'],
            'gender'    => ['required', 'enum:male,female']
        ]);
        
        if ($data['role_id'] == \Varhall\Framework\Model\Services\Framework\Users\RolesService::SUPER_ADMIN_ROLE)
            $this->checkSimplePermission('System:Users', 'superadmin');
        
        if ($this->quotaExhausted('users', $this->_usersService->findAll()->count()))
            $this->sendJsonError('Users quota exhausted', \Nette\Http\Response::S402_PAYMENT_REQUIRED);
        
        if ($this->isSaas())
            $data['customer_id'] = $this->currentCustomerId();

        if ($this->user->isAllowed('System:Users', 'superadmin') && isset($original['customer_id']))
            $data['customer_id'] = $original['customer_id'];
        
        $item = $this->_usersService->insert($data);
        $this->sendJson($item);
    }
    
    public function renderRestUpdate($id, array $data)
    {
        $data = $this->filterRequestData($data, ['email', 'password', 'name', 'surname', 'active', 'role_id', 'gender']);
        $this->validateRequestData($data, [
            'email'     => 'email',
            'password'  => 'string:6..',
            'role_id'   => 'int:1..',
            'gender'    => 'enum:male,female'
        ]);
    
        if ($data['role_id'] == \Varhall\Framework\Model\Services\Framework\Users\RolesService::SUPER_ADMIN_ROLE)
            $this->checkSimplePermission('System:Users', 'superadmin');
        
        $item = $this->_usersService->update($id, $data);
        $this->sendJson('ok');
    }
    
    public function renderRestDelete($id)
    {
        $this->_usersService->delete($id);
        $this->sendJson('ok');
    }
    
    protected function expandDefinition()
    {
        return [
            'role'  => 'ref:roles:role_id'
        ];
    }
}
