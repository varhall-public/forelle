<?php

namespace Varhall\Framework\Presenters\Framework;

/**
 * API Presenter urceny pro ziskavani uzivatelskych roli
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class RolesPresenter extends \Varhall\Framework\Presenters\Core\ApiPresenter
{
    /**
     * @var \Varhall\Framework\Model\Services\Framework\Users\RolesService
     */
    protected $_rolesService = NULL;
    
    public function __construct(\Varhall\Framework\Model\Services\Framework\Users\RolesService $rolesService)
    {
        parent::__construct();
        
        $this->_rolesService = $rolesService;
    }
    
    public function renderRestList()
    {
        if ($this->user->isAllowed('System:Users', 'superadmin'))
            $this->sendJson($this->_rolesService->findAll());
        else
            $this->sendJson($this->_rolesService->findCustomerRoles());
    }
    
    public function renderRestGet($id)
    {
        $role = $this->_rolesService->findById($id);
        
        if (!$this->user->isAllowed('System:Users', 'superadmin') && $role->name == \Varhall\Framework\Model\Services\Framework\Users\RolesService::SUPER_ADMIN_ROLE)
            $this->sendJson(NULL);
        else
            $this->sendJson($role);
    }
}
