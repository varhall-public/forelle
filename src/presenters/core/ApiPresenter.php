<?php

namespace Varhall\Framework\Presenters\Core;

/**
 * Based presenter used for all REST API presenters
 *
 * @author Ondrej Sibrava <ondrej.sibrava@varhall.cz>
 */
class ApiPresenter extends BasePresenter
{
    public function startup()
    {
        parent::startup();
        
        $this->autoCanonicalize = FALSE;
        
        if ($this->enabledAjaxOnly() && !$this->isAjax())
            throw new \Nette\Application\BadRequestException('Request must be AJAX only');
        
        if ($this->enabledPermissionsChecking())
            $this->checkPermissions();
    }
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    //// configuration                                                      ////
    ////////////////////////////////////////////////////////////////////////////
    
    protected function enabledAjaxOnly()
    {
        return TRUE;
    }
    
    protected function enabledPermissionsChecking()
    {
        return TRUE;
    }
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    //// default REST methods                                               ////
    ////////////////////////////////////////////////////////////////////////////
    
    public function renderRestList()
    {
        $this->sendJsonError('Method is unsupported', \Nette\Http\Response::S405_METHOD_NOT_ALLOWED);
    }

    public function renderRestGet($id)
    {
        $this->sendJsonError('Method is unsupported', \Nette\Http\Response::S405_METHOD_NOT_ALLOWED);
    }

    public function renderRestCreate(array $data)
    {
        $this->sendJsonError('Method is unsupported', \Nette\Http\Response::S405_METHOD_NOT_ALLOWED);
    }

    public function renderRestUpdate($id, array $data)
    {
        $this->sendJsonError('Method is unsupported', \Nette\Http\Response::S405_METHOD_NOT_ALLOWED);
    }

    public function renderRestDelete($id)
    {
        $this->sendJsonError('Method is unsupported', \Nette\Http\Response::S405_METHOD_NOT_ALLOWED);
    }
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    //// checks                                                             ////
    ////////////////////////////////////////////////////////////////////////////
    
    protected function checkPermissions()
    {
        $privilege = strtolower($this->getRequest()->getParameter('action'));
        $privilege = preg_replace('/^rest/', '', $privilege);
        
        if (!$this->user->isAllowed($this->getRequest()->getPresenterName(), $privilege))
            $this->sendJsonError('Forbidden', \Nette\Http\Response::S403_FORBIDDEN);
    }
    
    protected function checkSimplePermission($resource, $privilege)
    {
        $this->check(function($r, $p) {
            return $this->user->isAllowed($r, $p);
        }, [$resource, $privilege]);
    }
    
    protected function check($callback, array $params = [])
    {
        if (!is_callable($callback) || !call_user_func_array($callback, $params))
            $this->sendJsonError('Forbidden', \Nette\Http\Response::S403_FORBIDDEN);    
    }
}
