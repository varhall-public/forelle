<?php

namespace Varhall\Framework\Presenters\Core;

/**
 * Zakladni presenter pro souborovy modul
 * 
 * @author Ondrej Sibrava <ondrej.sibrava@varhall.cz>
 */
abstract class FilesBasePresenter extends \App\Presenters\BasePresenter
{
    /**
     * @var \Varhall\Framework\Model\Services\Framework\Files\FilesService
     */
    protected $_filesService = NULL;

    public function injectFilesService(\Varhall\Framework\Model\Services\Framework\Files\FilesService $service)
    {
        $this->_filesService = $service;
    }
    
    public function startup()
    {
        parent::startup();
        
        $this->autoCanonicalize = FALSE;
        
        if ($this->enabledPermissionsChecking())
            $this->checkPermissions();
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //// configuration                                                      ////
    ////////////////////////////////////////////////////////////////////////////
    
    protected function enabledPermissionsChecking()
    {
        return TRUE;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //// default file methods                                               ////
    ////////////////////////////////////////////////////////////////////////////
    
    public function renderUpload(array $data, array $files)
    {
        throw new \Nette\NotSupportedException('Upload method is not supported');
    }
    
    public function renderDownload($id, $version = NULL)
    {
        throw new \Nette\NotSupportedException('Download method is not supported');
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //// shared methods                                                     ////
    ////////////////////////////////////////////////////////////////////////////
    
    protected function checkPermissions()
    {
        $privilege = strtolower($this->getRequest()->getParameter('action'));
        $privilege = preg_replace('/^rest/', '', $privilege);
        
        if (!$this->user->isAllowed($this->getRequest()->getPresenterName(), $privilege))
            $this->sendJsonError('Forbidden', \Nette\Http\Response::S403_FORBIDDEN);
    }
    
    
    protected function sendFile(\Nette\Database\Table\ActiveRow $file, $version = NULL, $name = NULL)
    {
        $item = $this->_filesService->readFile($file->id, $version);
        
        $response = new \Nette\Application\Responses\FileResponse($item['data'], !empty($name) ? $name : $item['file']->name, $item['version']->mime_type, FALSE);
        $this->sendResponse($response);
    }
}
