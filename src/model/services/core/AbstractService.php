<?php

namespace Varhall\Framework\Model\Services\Core;

/**
 * Description of AbstractService
 *
 * @author fero
 * 
 * @method \Nette\Database\Table\Selection findAll()
 * @method \Nette\Database\Table\IRow findById($id)
 * @method \Nette\Database\Table\Selection findByParams(array $params)
 * @method \Nette\Database\Table\IRow insert(array $data)
 * @method mixed update($id, array $data)
 * @method \Nette\Database\Table\IRow delete($id)
 * 
 */
abstract class AbstractService
{
    /**
     * @var \Varhall\Framework\Model\Tables\Core\AbstractTable
     */
    private $_masterTable = NULL;
    
    public function __construct(\Varhall\Framework\Model\Tables\Core\AbstractTable $masterTable)
    {
        $this->_masterTable = $masterTable;
    }
    
    
    /// PUBLIC METHODS
    
    public function __call($name, $args)
    {
        return call_user_func_array([$this->getMasterTable(), $name], $args);
        //return \Nette\Utils\ObjectMixin::call($this->getMasterTable(), $name, $args);
    }
    
    
    /// PROTECTED METHODS

    /**
     * Gets table which service primary working with
     *
     * @return \Varhall\Framework\Model\Tables\Core\AbstractTable
     */
    protected function getMasterTable()
    {
        return $this->_masterTable;
    }
}
