<?php

namespace Varhall\Framework\Model\Services\Framework\Customers;

/**
 * Description of CustomersService
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 * 
 * @method \Nette\Database\Table\IRow findByDomain($domain)
 */
class CustomersService extends \Varhall\Framework\Model\Services\Core\AbstractService
{
    public function __construct(\Varhall\Framework\Model\Tables\Framework\Customers\CustomersTable $customersTable)
    {
        parent::__construct($customersTable);
    } 
    
    public function isCustomerEnabled($customer)
    {
        return boolval($customer->active) && $customer->paid_until > new \Nette\Utils\DateTime();
    }
}
