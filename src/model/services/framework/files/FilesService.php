<?php

namespace Varhall\Framework\Model\Services\Framework\Files;

/**
 * @author Jan Kubena <kubena@varhall.cz>
 */
class FilesService extends \Varhall\Framework\Model\Services\Core\AbstractService
{
    

    /**
     * @var \Varhall\Framework\Model\Files\FilesRepository
     */
    private $_filesRepository = NULL;
    
    /**
     * @var \Varhall\Framework\Model\Tables\Framework\Files\FilesTable 
     */
    private $_filesTable = NULL;
    
    /**
     * @var \Varhall\Framework\Model\Tables\Framework\Files\FileVersionsTable 
     */
    private $_fileVersionsTable = NULL;

    public function __construct(
            \Varhall\Framework\Model\Tables\Framework\Files\FilesTable $filesTable,
            \Varhall\Framework\Model\Tables\Framework\Files\FileVersionsTable $fileVersionsTable,
            \Varhall\Framework\Model\Files\FilesRepository $filesRepository)
    {
        parent::__construct($filesTable);

        $this->_filesRepository = $filesRepository;
        
        $this->_filesTable = $filesTable;
        $this->_fileVersionsTable = $fileVersionsTable;
    }
    
    
    
    
    // PUBLIC METHODS //
    

    /**
     * Saves file from FileUpload
     *
     * @param Nette\Http\FileUpload $file
     * @return bool|int|Nette\Database\Table\IRow
     * @throws Nette\Application\BadRequestException
     */
    public function saveFile(\Nette\Http\FileUpload $file)
    {
        $type = FileTypes::findByMimeType($file->getContentType());

        $info = [];

        if ($type == FileTypes::IMAGE)
            $info = $this->_filesRepository->saveImage($file);
        
        else if ($type == FileTypes::AUDIO)
            $info = $this->_filesRepository->saveAudio($file);
        
        else if ($type == FileTypes::VIDEO)
            $info = $this->_filesRepository->saveVideo($file);
        
        else
            $info = $this->_filesRepository->saveFile($file);

        if (empty($info) || !is_array($info))
            throw new \Nette\InvalidStateException('No files to save');
        
        // check primary files
        $this->definePrimaryFile($info);
        
        // save file
        $fileItem = $this->_filesTable->insert([
            'name'  => $file->name,
            'type'  => $type
        ]);
        
        // save versions
        foreach ($info as $version) {
            $version['file_id'] = $fileItem->id;
            $this->_fileVersionsTable->insert($version);
        }
        
        return $fileItem;
    }

    public function readFile($id, $versionType = NULL)
    {
        $info = $this->_filesTable->findById($id);
        
        if (!$info)
            throw new \Nette\InvalidArgumentException('File not found');
        
        $versions = $info->related('file_versions', 'file_id');
        
        if ($versionType === NULL)
            $versions->where('primary', TRUE);
        else
            $versions->where('type', $versionType);
        
        if (count($versions) == 0)
            throw new Nette\InvalidArgumentException('Required file version not found');
        
        $version = $versions->fetch();
        
        return [
            'file'      => $info,
            'version'   => $version,
            'data'      => $this->_filesRepository->readFile($version->path)
        ];
    }

    public function delete($id, $fromDisk = FALSE)
    {
        // TODO: implement delete from disk
        return parent::delete($id);
    }

    // PROTECTED METHODS //
    
    protected function definePrimaryFile(array &$files)
    {
        $hasPrimary = FALSE;
        
        foreach ($files as $file) {
            if (isset($file['primary']) && $file['primary'] === TRUE) {
                $hasPrimary = TRUE;
                break;
            }
        }
        
        if (!$hasPrimary)
            $files[0]['primary'] = TRUE;
        
        return $files;
    }
}