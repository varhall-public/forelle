<?php

namespace Varhall\Framework\Model\Services\Framework\Emails;


/**
 * @author Jan Kubena <kubena@varhall.cz>
 * 
 * @method \Nette\Database\Table\Selection findByUserId($userId)
 */
abstract class AbstractEmailsService extends \Varhall\Framework\Model\Services\Core\AbstractService
{
    /**
     * @var \Nette\Mail\IMailer
     */
    private $_mailer = NULL;
    
    /**
     * @var \Varhall\Framework\Model\Tables\Framework\Emails\EmailsTable
     */
    private $_emailsTable = NULL;
    
    private $_senderName = 'Reservando';
    
    private $_senderEmail = 'system@reservando.cz';
    
    private $_templateDir = '';

    private $_httpRequest = NULL;
    
    
    public function __construct(
            \Nette\DI\Container $container,
            \Nette\Mail\IMailer $mailer,
            \Varhall\Framework\Model\Tables\Framework\Emails\EmailsTable $emailsTable,
            \Nette\Http\IRequest $httpRequest = NULL)
    {
        parent::__construct($emailsTable);

        $this->_templateDir = $container->parameters['appDir'] . '/presenters/templates/emails';
        $this->_senderName = $container->parameters['email']['senderName'];
        $this->_senderEmail = $container->parameters['email']['senderEmail'];
        
        $this->_mailer = $mailer;
        $this->_emailsTable = $emailsTable;
        $this->_httpRequest = $httpRequest;
    }

    // PUBLIC METHODS //
    
    protected function createTemplate()
    {
        return new \Latte\Engine();
    }
    
    protected function sendMessage($recipient, $subject, $template, array $data)
    {
        $latte = $this->createTemplate();
        
        $data = array_merge($data, [
            'baseUri' => $this->_httpRequest ? rtrim($this->_httpRequest->getUrl()->getBaseUrl(), '/') : null
        ]);

        $html = $latte->renderToString($this->_templateDir . "/{$template}.html.latte", $data);
        $plain = $latte->renderToString($this->_templateDir . "/{$template}.plain.latte", $data);
   
        // send email
        
        $mail = new \Nette\Mail\Message;
        $mail->setFrom($this->_senderEmail, $this->_senderName)
            ->addTo($recipient)
            ->setSubject("[Reservando] {$subject}")
            ->setBody($plain)
            ->setHtmlBody($html);
    
        $this->_mailer->send($mail);
        
        // save email
        
        $this->_emailsTable->insert([
            'recipient'     => $recipient,
            'subject'       => $subject,
            'html_content'  => $html,
            'plain_content' => $plain,
            'raw_data'      => \Nette\Utils\Json::encode($data),
            'template'      => $template
        ]);
    }
}
