<?php

namespace Varhall\Framework\Model\Services\Framework\Users;

use Nette\InvalidArgumentException;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;

/**
 * @author Jan Kubena <kubena@varhall.cz>
 * 
 * @method \Nette\Database\Table\IRow findByEmail($email)
 * @method \Nette\Database\Table\IRow findActiveByEmail($email)
 * @method \Nette\Database\Table\Selection findByRoleId($roleId)
 * @method \Nette\Database\Table\Selection findActive()
 * @method \Nette\Database\Table\Selection findInactive()
 */
class UsersService extends \Varhall\Framework\Model\Services\Core\AbstractService implements IAuthenticator
{
    // CONSTRUCTOR //
    
    public function __construct(\Varhall\Framework\Model\Tables\Framework\Users\UsersTable $usersTable)
    {
        parent::__construct($usersTable);
    }
    
    
    
    // PUBLIC METHODS //

    /**
     * Performs an authentication against e.g. database.
     * and returns IIdentity on success or throws AuthenticationException
     *
     * @param array $credentials
     * @return IIdentity
     * @throws AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;
        
        if (!isset($email) || !isset($password))
            throw new InvalidArgumentException('Email and password must be specified');

        $user = $this->findByEmail($email);

        if (!$user)
            throw new AuthenticationException('Email does not exists', self::IDENTITY_NOT_FOUND);
        
        if (!$user->active)
            throw new AuthenticationException('User is disabled', self::NOT_APPROVED);
        
        if (!Passwords::verify($password, $user->password))
            throw new AuthenticationException('Invalid password', self::INVALID_CREDENTIAL);
        
        $role = $user->ref($this->getRoleTableName(), 'role_id')->name;
        $identityData = $user->toArray();
        unset($identityData['password']);
        
        return new Identity($user['id'], $role,  $identityData);
    }
    
    /**
     * Verifies if user with given ID has given password
     * 
     * @param int $userId
     * @param string $password
     * @return bool
     */
    public function verifyPassword($userId, $password)
    {
        $user = $this->findById($userId);
        
        return Passwords::verify($password, $user->password);
    }
    

    // PROTECTED METHODS //
    
    protected function getRoleTableName()
    {        
        return 'roles';
    }
}