<?php

namespace Varhall\Framework\Model\Services\Framework\Users;

/**
 * Description of RolesService
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class RolesService extends \Varhall\Framework\Model\Services\Core\AbstractService
{
    const SUPER_ADMIN_ROLE = 'admin';
    
    public function __construct(\Varhall\Framework\Model\Tables\Framework\Users\RolesTable $rolesTable)
    {
        parent::__construct($rolesTable);
    }
    
    /**
     * Ziska vsechny role, krome super-administratorske
     * 
     * @return \Nette\Database\Table\Selection
     */
    public function findCustomerRoles()
    {
        return $this->findAll()->where('name <> ?', self::SUPER_ADMIN_ROLE);
    }
}
