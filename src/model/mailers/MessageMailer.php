<?php

namespace Varhall\Framework\Model\Mailers;

/**
 * Description of MessageMailer
 *
 * @author Ondrej Sibrava <ondrej.sibrava@varhall.cz>
 */
class MessageMailer extends AbstractMailer
{ 
    // PUBLIC METHODS //


    // PROTECTED METHODS //
    
    protected function getTemplateName()
    {
        return 'message';
    }
}
