<?php

namespace Varhall\Framework\Model\Mailers;

use Nette\Application\LinkGenerator;
use Nette\Application\UI\ITemplate;
use Nette\Application\UI\ITemplateFactory;
use Nette\DI\Container;
use Nette\Mail\IMailer;
use Nette\Mail\Message;

/**
 * Description of AbstractMailer
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
abstract class AbstractMailer
{
    /**
     * @var IMailer
     */
    protected $_mailer = NULL;
    
    /**
     * @var string
     */
    protected $_templateDir = NULL;
    
    /**
     * @var ITemplate 
     */
    protected $_latte = NULL;
    
    /**
     * @var string
     */
    protected $_config = [
        'from'      => 'admin@varhall.cz'
    ];
    
    /**
     * Pole funkci, provadenych behem odesilani
     * 
     * @var array 
     */
    public $onSent = [];
    
    public function __construct(IMailer $mailer, Container $container, ITemplateFactory $factory, LinkGenerator $linkGenerator)
    {
        $this->_mailer = $mailer;
        
        $params = $container->getParameters();
        $this->_templateDir = $params['appDir'] . '/presenters/templates/emails/';
        
        $this->_latte = $factory->createTemplate();
        $this->_latte->_control = $linkGenerator;
        
        if (isset($container->parameters['mail']))
            $this->_config = $container->parameters['mail'];
    }
    
    public function sendEmail($recipient, $subject, $data, $userId = NULL)
    {
        $this->_latte->recipient = $recipient;
        $this->_latte->subject = $subject;
        $this->setTemplateData($this->_latte, $data);
        
        $this->_latte->setFile($this->_templateDir . $this->getTemplateName() . '.latte');
        $html = (string) $this->_latte;
        
        $subjectPrefix = trim($this->_config['subject_prefix']);
        if (!empty($subjectPrefix))
            $subjectPrefix .= ' ';
        
        // send mail to 
        $mail = new Message();
        $mail->setFrom($this->_config['from'])
            ->addTo($recipient)
            ->setSubject($subjectPrefix . $subject)
            ->setHtmlBody($html);
        
        $this->_mailer->send($mail);
        
        $this->raiseEvents($recipient, $subject, $data, $userId, $html, $this->getTemplateName());
    }
    
    
    // PROTECTED METHODS //
    
    protected abstract function getTemplateName();
    
    protected function setTemplateData(\Nette\Application\UI\ITemplate &$template, $data)
    {
        $template->data = $data;
    }
    
    
    // PRIVATE METHODS
    
    private function raiseEvents($recipient, $subject, $data, $userId, $html, $templateName)
    {
        if ($this->onSent === NULL)
            return;
        
        if (!is_array($this->onSent) && !$this->onSent instanceof \Traversable) {
            throw new \Nette\UnexpectedValueException('Property AbstractMailer::$onSent must be array or Traversable, ' . gettype($this->onSent) . ' given.');
        }
        
        foreach ($this->onSent as $handler) {
            $params = \Nette\Utils\Callback::toReflection($handler)->getParameters();
            
            \Nette\Utils\Callback::invoke($handler, $recipient, $subject, $data, $userId, $html, $templateName);
        }
    }

}
