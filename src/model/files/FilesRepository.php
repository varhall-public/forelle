<?php

namespace Varhall\Framework\Model\Files;

use Nette\Http\FileUpload;
use Nette\InvalidArgumentException;
use Nette\Utils\DateTime;
use Nette\Utils\Image;

/**
 * @author Jan Kubena <kubena@varhall.cz>
 */
class FilesRepository
{
    const IMAGES_PATH = '/files/images';
    
    const FILES_PATH = '/files';

    protected $_basePath = '';

    protected $_imageSizes = [
        [ 'width' =>  1280, 'height' => 720, 'type' => 'large', 'primary' => TRUE ],
        [ 'width' =>  640,  'height' => 360, 'type' => 'medium' ],
        [ 'width' =>  720,  'height' => 720, 'type' => 'large_rect', 'resize' => Image::EXACT ],
        [ 'width' =>  360,  'height' => 360, 'type' => 'medium_rect', 'resize' => Image::EXACT ],
    ];
    
    // PUBLIC METHODS //

    public function __construct(\Nette\DI\Container $container)
    {
        $this->_basePath = $container->parameters['wwwDir'];
    }

    public function saveFile(FileUpload $file)
    {
        return $this->saveCommonFile($file);
    }
    
    public function saveAudio(FileUpload $file)
    {
        return $this->saveCommonFile($file);
    }

    public function saveVideo(FileUpload $file)
    {
        return $this->saveCommonFile($file);
    }

    public function saveImage(FileUpload $file)
    {
        return $this->saveCommonFile($file, 'getImageNames', $this->_imageSizes);
    }

    public function readFile($filename)
    {
        $filename = $this->_basePath . $filename;
                
        if (!file_exists($filename))
            throw new InvalidArgumentException('Given file does not exist');
        
        return new \SplFileInfo($filename);
    }
    
    public function removeFile($filename)
    {
        if (file_exists($filename))
            unlink($filename);
    }

    // PROTECTED METHODS
    
    protected function saveCommonFile(FileUpload $file, $filenamesCallback = NULL, array $args = [])
    {
        $files = [];
        
        $storage = $this->getStorage();
        $filenames = !empty($filenamesCallback) ? $this->$filenamesCallback($file) : $this->getFileNames($file);

        foreach ($filenames as $index => $filename) {
            $storeArgs = isset($args[$index]) ? $args[$index] : [];
            
            $files[] = $this->store($file, $filename, $storage, $storeArgs);
        }
        
        return $files;
    }
    
    protected function store(FileUpload $file, $filename, $storage, $args = [])
    {
        $path = implode('/', [$storage, $filename ]);
        
        $data = [
            'path'          => $path,
            'size'          => $file->size,
            'mime_type'     => $file->contentType,
            'type'          => isset($args['type']) ? $args['type'] : '',
            'primary'       => (isset($args['primary']) && $args['primary'] === TRUE),
            'parameters'    => $args
        ];
        
        $absolutePath = $this->_basePath . $path;
        
        if ($file->isImage())
            $this->storeImage($file, $absolutePath, $args);
        else
            $this->storeFile($file, $absolutePath, []);
     
        return $data;
    }
    
    protected function storeImage(FileUpload $file, $path, array $args)
    {
        if (!$file->isImage())
            throw new InvalidArgumentException('Given file is not image');
        
        $image = $file->toImage();
        
        $width  = (isset($args['width'])  && is_int($args['width']))    ? $args['width']  : NULL;
        $height = (isset($args['height']) && is_int($args['height']))   ? $args['height'] : NULL;
        
        if ($width != NULL || $height != NULL)
            $image->resize($width, $height, (isset($args['resize']) ? $args['resize'] : NULL));

        $image->save($path);
    }
    
    protected function storeFile(FileUpload $file, $path, array $args)
    {
        file_put_contents($path, $file->contents);
    }
    
    protected function getStorage()
    {
        $date = new DateTime();
        
        $directory = implode('/', [ self::FILES_PATH, $date->format('Y'), $date->format('m') ]);
    
        $absoluteDirectory = $this->_basePath . $directory;
        
        if (!file_exists($absoluteDirectory))
            mkdir($absoluteDirectory, 0777, TRUE);
        
        return $directory;
    }
    
    protected function getFileNames(FileUpload $file, array $suffixes = [])
    {
        $names = [];
        
        if (empty($suffixes))
            $suffixes[] = NULL;

        $parts = explode('.', $file->getName());
        $extension = array_pop($parts);
        $baseName = (new DateTime())->format('YmdHis') . rand(100, 999);
        
        foreach ($suffixes as $suffix) {
            $sfPart = !empty($suffix) ? '_' . $suffix : '';
            $names[] = $baseName . $sfPart . '.' . $extension;
        }
        
        return $names;
    }
    
    protected function getImageNames(FileUpload $file)
    {
        $suffixes = [];
        foreach ($this->_imageSizes as $size) {
            $suffixes[] = (isset($size['width']) && isset($size['height']))  
                            ? $size['width'] . 'x' . $size['height']
                            : NULL;
        }
        
        return $this->getFileNames($file, $suffixes);
    }
}
