<?php

namespace Varhall\Framework\Model\Services\Framework\Files;

/**
 * @author Jan Kubena <kubena@varhall.cz>
 */
class FileTypes
{
    const
        DIRECTORY   = 'directory',
        UNKNOWN     = 'unknown',
        IMAGE       = 'image',
        VIDEO       = 'video',
        AUDIO       = 'audio';

    /**
     * Find file type by MIME type
     *
     * @param $type
     * @return int|mixed
     */
    public static function findByMimeType($mimeType)
    {
        $mimeType = explode('/', $mimeType)[0];

        $type = self::findByName($mimeType);
        if (is_null($type))
            return self::UnknownFile;

        return $type;
    }

    public static function findByName($name)
    {
        $const = self::class . '::' . strtoupper($name);
        
        if (defined($const)) {
            return constant($const);
            
        } else {
            return FileTypes::UnknownFile;
        }
    }
}