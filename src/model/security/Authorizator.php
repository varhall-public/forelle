<?php

namespace Varhall\Framework\Model\Security;

/**
 * Description of Authorizator
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
abstract class Authorizator
{
    protected $_user = NULL;
    
    public function __construct(\Nette\Security\User $user)
    {
        $this->_user = $user;
    }    
    
    
    /// PUBLIC METHODS
    
}
