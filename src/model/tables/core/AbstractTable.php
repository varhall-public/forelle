<?php

namespace Varhall\Framework\Model\Tables\Core;

/**
 * Abstract base database table
 *
 * @author Jan Kubena <kubena@varhall.cz>
 */
abstract class AbstractTable
{
    /** @var \Nette\Database\Context */
    protected $_database = NULL;
    
    /**
     * @var array
     */
    private $_plugins = [];

    public function __construct(\Nette\Database\Context $database)
    {
        $this->_database = $database;
    }

    // PUBLIC METHODS //

    /**
     * Finds all rows
     *
     * @return \Nette\Database\Table\Selection
     */
    public function findAll()
    {
        $table = $this->getTable();

        return $table;
    }

    /**
     * Finds row by id
     *
     * @param $id
     * @return \Nette\Database\Table\IRow
     */
    public function findById($id)
    {
        $object = $this->getTable()->get($id);
        
        return $object;
    }

    /**
     * Finds selection based on params
     *
     * @param array $params
     * @return \Nette\Database\Table\Selection
     */
    public function findByParams(array $params)
    {
        $selection = $this->findAll();

        foreach ($params as $key => $param) {
            $selection->where($key, $param);
        }

        return $selection;
    }

    /**
     * Inserts new row
     *
     * @param array $data
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function insert(array $data)
    {
        $this->callPlugins('beforeInsert', [ &$data ]);

        return $this->getTable()->insert($data);
    }

    /**
     * Updates row
     *
     * @param $id
     * @param array $data
     * @return null
     */
    public function update($id, array $data)
    {
        if ($this->findById($id) === FALSE) {
            return NULL;
        }
        
        $this->callPlugins('beforeUpdate', [ $id, &$data ]);

        return $this->findById($id)->update($data);
    }

    /**
     * Deletes row
     *
     * @param $id
     * @return int|null
     */
    public function delete($id)
    {
        $item = $this->findById($id);
        if ($item === FALSE) {
            return NULL;
        }
        
        $this->callPlugins('beforeDelete', [ $id, $item ]);

        $cancel = FALSE;
        foreach ($this->_plugins as $plugin) {
            $callback = [$plugin, 'cancelDelete'];
            
            if (is_callable($callback))
                $cancel = $cancel || call_user_func($callback);
        }
        
        if (!$cancel)
            $item->delete();
        
        return $item;
    }

    // PROTECTED METHODS //

    /**
     * Gets the name of table it's working with
     *
     * @return mixed
     */
    protected abstract function getTableName();

    /**
     * Gets a table from database with name specified in "getTableName" method
     *
     * @return \Nette\Database\Table\Selection
     */
    protected function getTable()
    {
        $table = $this->_database->table($this->getTableName());
        $table = new Database\PluginSelection($table, $this->_plugins);
        
        $this->callPlugins('filterResults', [ &$table ]);
        
        return $table;
    }
    
    /**
     * Registers a plugin into the service
     * 
     * @param \App\Model\Services\Core\Plugins\ServicePlugin $plugin
     */
    protected function addPlugin(Plugins\ServicePlugin $plugin)
    {
        $this->_plugins[] = $plugin;
    }
    
    /**
     * Calls method on each enabled plugin with given args
     * 
     * @param string $function Function name
     * @param array $args Array of arguments which are passed into a function
     */
    protected function callPlugins($function, array $args)
    {
        foreach ($this->_plugins as $plugin) {
            $callback = [$plugin, $function];
            
            if (is_callable($callback))
                call_user_func_array($callback, $args);
        }
    }
}