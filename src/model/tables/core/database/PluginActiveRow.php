<?php

namespace Varhall\Framework\Model\Tables\Core\Database;

/**
 * Description of PluginActiveRow
 *
 * @author fero
 */
class PluginActiveRow extends \Nette\Database\Table\ActiveRow
{
    /**
     * @var array
     */
    protected $_plugins = [];
    
    public function __construct(array $data, \Nette\Database\Table\Selection $table, array $plugins = NULL)
    {
        parent::__construct($data, $table);
        
        if ($plugins)
            $this->_plugins = $plugins;
    }
    
    public function &__get($key)
    {
        $value = NULL;
        try {
            $value = parent::__get($key);  
            
        } catch (\Nette\MemberAccessException $ex) {
            $this->accessColumn(NULL);
            $value = parent::__get($key);
        }
        
        $value = $this->readField($key, $value);
        return $value;
    }
    
    public function toArray()
    {
        $values = parent::toArray();
        
        if (!$values || !is_array($values)) {
            throw new \Nette\InvalidStateException('Unable convert row to array');
        }
        
        foreach ($values as $key => $value) {
            $values[$key] = $this->readField($key, $value);
        }
        
        return $values;
    }
    
    protected function readField($key, &$value)
    {
        foreach ($this->_plugins as $plugin) {
            $value = $plugin->readField($key, $value);
        }
        
        return $value;
    }
}
