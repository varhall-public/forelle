<?php

namespace Varhall\Framework\Model\Tables\Core\Database;

/**
 * Description of PluginSelection
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class PluginSelection extends \Nette\Database\Table\Selection
{
    /**
     * @var array
     */
    protected $_plugins = [];
    
    public function __construct(\Nette\Database\Table\Selection $selection, array $plugins = NULL)
    {
        parent::__construct($selection->context, $selection->conventions, $selection->name, NULL);
        $this->cache = $selection->cache;
        
        if ($plugins)
            $this->_plugins = $plugins;
    }
    
    protected function createRow(array $row)
    {
        return new PluginActiveRow($row, $this, $this->_plugins);
    }
    
    public function insert($data)
    {
        $row = parent::insert($data);

        if ($row instanceof \Nette\Database\Table\Selection) {
            return new PluginSelection($row, $this->_plugins);
        
        } else if ($row instanceof \Nette\Database\Table\ActiveRow) {
            $table = $this->readPrivateProperty($row, 'table');
            $data  = $this->readPrivateProperty($row, 'data');
            
            return new PluginActiveRow($data, $table, $this->_plugins);
        }
        
        return $row;
    }
    
    protected function decorate(\Nette\Database\Table\ActiveRow $row)
    {
        foreach ($this->_plugins as $plugin) {
            $row = $plugin->decorateRow($row);
        }
        
        return $row;
    }
    
    private function readPrivateProperty($object, $property)
    {
        $value = \Closure::bind(function() use ($property) {
            return $this->$property;
        }, $object, $object)->__invoke();

        return $value;
    }
}
