<?php

namespace Varhall\Framework\Model\Tables\Core\Plugins;

/**
 * Description of JsonPlugin
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class JsonPlugin extends ServicePlugin
{
    private $_jsonFields = [];
    
    public function __construct(array $jsonFields = [])
    {
        foreach ($jsonFields as $field)
            $this->addJsonField($field);
    }
    
    public function addJsonField($jsonField)
    {
        $this->_jsonFields[] = $jsonField;
    }
    
    
    /// PLUGIN METHODS
    
    public function readField($field, $value)
    {
        if (in_array($field, $this->_jsonFields) && empty($value))
            return [];
        
        if (in_array($field, $this->_jsonFields) && is_string($value))
            $value = \Nette\Utils\Json::decode($value, \Nette\Utils\Json::FORCE_ARRAY);
        
        return $value;
    }
    
    public function beforeInsert(array &$data)
    {
        $this->encodeFields($data);
        
        return $data;
    }
    
    public function beforeUpdate($id, array &$data)
    {
        $this->encodeFields($data);
    }


    /// PRIVATE & PROTECTED METHODS
    
    private function encodeFields(array &$data)
    {
        foreach ($this->_jsonFields as $field) {
            if (isset($data[$field]) && is_array($data[$field]))
                $data[$field] = json_encode($data[$field]);
        }
    }
}
