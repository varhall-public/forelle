<?php

namespace Varhall\Framework\Model\Tables\Core\Plugins;

/**
 * Description of IServicePlugin
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
abstract class ServicePlugin
{
    public function filterResults(\Nette\Database\Table\Selection &$table)
    {
        
    }
    
    public function readField($field, $value)
    {
        return $value;
    }
    
    public function beforeInsert(array &$data)
    {
        
    }
    
    public function beforeUpdate($id, array &$data)
    {
        
    }
    
    public function beforeDelete($id, \Nette\Database\Table\ActiveRow $item)
    {
        
    }
    
    public function cancelDelete()
    {
        return FALSE;
    }
}
