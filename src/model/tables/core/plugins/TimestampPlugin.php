<?php

namespace Varhall\Framework\Model\Tables\Core\Plugins;

/**
 * Description of ScopePlugin
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class TimestampPlugin extends ServicePlugin
{
    const FIELD_CREATED = 'created_at';
    
    const FIELD_UPDATED = 'updated_at';
    
    const FIELD_DELETED = 'deleted_at';
    
    /**
     * @var string
     */
    private $_createdAt = self::FIELD_CREATED;
    
    /**
     * @var string
     */
    private $_updatedAt = self::FIELD_UPDATED;
    
    /**
     * @var string
     */
    private $_deletedAt = self::FIELD_DELETED;
    
    /**
     * Pole sloupcu, ktere urcuji podmnozinu radku v tabulce<br>
     * Pole je definovane jako asociativni pole ve tvaru [ sloupec => hodnota ]<br>
     * <br>
     * <b>Priklad:</b><br>
     * [ customer_id => 1 ]<br>
     * 
     * @param array $scopeFields
     */
    public function __construct($createdAt = self::FIELD_CREATED, $updatedAt = self::FIELD_UPDATED, $deletedAt = self::FIELD_DELETED)
    {
        $this->_createdAt = $createdAt;
        $this->_updatedAt = $updatedAt;
        $this->_deletedAt = $deletedAt;
    }

    
     /// PLUGIN METHODS
    
    public function filterResults(\Nette\Database\Table\Selection &$table)
    {
        $table->where($table->getName() . '.' . $this->_deletedAt, NULL);
    }
    
    public function beforeInsert(array &$data)
    {
        $data[$this->_createdAt] = new \Nette\Utils\DateTime();
        $data[$this->_updatedAt] = NULL;
        
        return $data;
    }
    
    public function beforeUpdate($id, array &$data)
    {
        $data[$this->_updatedAt] = new \Nette\Utils\DateTime();
    }
    
    public function beforeDelete($id, \Nette\Database\Table\ActiveRow $item)
    {
        $item->update([
            $this->_deletedAt => new \Nette\Utils\DateTime()
        ]);
    }
    
    public function cancelDelete()
    {
        return TRUE;
    }
    
    /// PRIVATE & PROTECTED METHODS
    
}
