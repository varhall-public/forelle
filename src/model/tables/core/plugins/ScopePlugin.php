<?php

namespace Varhall\Framework\Model\Tables\Core\Plugins;

/**
 * Description of ScopePlugin
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class ScopePlugin extends ServicePlugin
{
    /**
     * @var array
     */
    private $_scopeFields = [];
    
    /**
     * Pole sloupcu, ktere urcuji podmnozinu radku v tabulce<br>
     * Pole je definovane jako asociativni pole ve tvaru [ sloupec => hodnota ]<br>
     * <br>
     * <b>Priklad:</b><br>
     * [ customer_id => 1 ]<br>
     * 
     * @param array $scopeFields
     */
    public function __construct(array $scopeFields = [])
    {
        foreach ($scopeFields as $field => $value)
            $this->setScopeField($field, $value);
    }
    
    public function setScopeField($field, $value)
    {
        if (!empty($value))
            $this->_scopeFields[$field] = $value;
    }
    
    
    
     /// PLUGIN METHODS
    
    public function filterResults(\Nette\Database\Table\Selection &$table)
    {
        foreach ($this->_scopeFields as $field => $value) {
            $table->where($field, $value);
        }
    }
    
    public function beforeInsert(array &$data)
    {
        $this->setRowData($data);
        
        return $data;
    }
    
    public function beforeUpdate($id, array &$data)
    {
        $this->setRowData($data);
    }
    
    /// PRIVATE & PROTECTED METHODS
    
    private function setRowData(array &$data)
    {
        foreach ($this->_scopeFields as $field => $value) {
            if (isset($data[$field]))
                $data[$field] = $value;
        }
    }
}
