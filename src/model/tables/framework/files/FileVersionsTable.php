<?php

namespace Varhall\Framework\Model\Tables\Framework\Files;

/**
 * Description of FileVersionsTable
 *
 * @author fero
 */
class FileVersionsTable extends \Varhall\Framework\Model\Tables\Core\AbstractTable
{
    public function __construct(\Nette\Database\Context $database)
    {
        parent::__construct($database);
        
        $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\TimestampPlugin());
        $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\JsonPlugin([ 'parameters' ]));
    }
    
    /// PUBLIC METHODS
  
    
    
    /// PROTECTED METHODS
    
    protected function getTableName()
    {
        return 'file_versions';
    }
}
