<?php

namespace Varhall\Framework\Model\Tables\Framework\Files;

/**
 * Description of FilesTable
 *
 * @author fero
 */
class FilesTable extends \Varhall\Framework\Model\Tables\Core\AbstractTable
{
    public function __construct(\Nette\Database\Context $database)
    {
        parent::__construct($database);
        
        $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\TimestampPlugin());
        $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\JsonPlugin([ 'parameters' ]));
    }
    
    /// PUBLIC METHODS
    
    /// PROTECTED METHODS
    
    protected function getTableName()
    {
        return 'files';
    }

}
