<?php

namespace Varhall\Framework\Model\Tables\Framework\Emails;

/**
 * Description of EmailsTable
 *
 * @author fero
 */
class EmailsTable extends \Varhall\Framework\Model\Tables\Core\AbstractTable
{
    public function __construct(\Nette\Database\Context $database)
    {
        parent::__construct($database);
        
        $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\TimestampPlugin());
    }
    
    /// PUBLIC METHODS
    
    /// PROTECTED METHODS
    
    protected function getTableName()
    {
        return 'emails';
    }
}
