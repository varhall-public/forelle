<?php

namespace Varhall\Framework\Model\Tables\Framework\Users;

/**
 * Description of RolesTable
 *
 * @author fero
 */
class RolesTable extends \Varhall\Framework\Model\Tables\Core\AbstractTable
{
    protected function getTableName()
    {
        return 'roles';
    }

}
