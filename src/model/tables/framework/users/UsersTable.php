<?php

namespace Varhall\Framework\Model\Tables\Framework\Users;

use Nette\InvalidArgumentException;
use Nette\Security\Passwords;

/**
 * @author Jan Kubena <kubena@varhall.cz>
 */
class UsersTable extends \Varhall\Framework\Model\Tables\Core\AbstractTable
{
    // CONSTRUCTOR //
    
    public function __construct(
            \Nette\DI\Container $container,
            \Nette\Database\Context $database, 
            \Varhall\Framework\Model\Tables\Framework\Customers\CustomerResolver $customerResolver
    )
    {
        parent::__construct($database);
        
        $saas = isset($container->parameters['saas']) ? $container->parameters['saas'] : FALSE;

        $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\TimestampPlugin());
        if ($saas) {
            $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\ScopePlugin([ 'customer_id' => $customerResolver->currentCustomerId() ]));
        }
    }
    
    
    
    // PUBLIC METHODS //

    /**
     * Inserts new user and hashes his password
     *
     * @param array $data
     * @return bool|int|\Nette\Database\Table\IRow|void
     * @throws InvalidArgumentException
     */
    public function insert(array $data)
    {
        if (!isset($data[$this->getLoginColumn()]))
            throw new InvalidArgumentException('Login column must be specified');

        if ($this->findByLoginColumn($data[$this->getLoginColumn()]))
            throw new InvalidArgumentException('Login column value is already in use');

        $data['password'] = Passwords::hash($data['password']);

        return parent::insert($data);
    }

    /**
     * Updates user's data and hashes his password
     *
     * @param $id
     * @param array $data
     * @return null|void
     * @throws InvalidArgumentException
     */
    public function update($id, array $data)
    {
        if (isset($data[$this->getLoginColumn()])) {
            $user = $this->findByLoginColumn($data[$this->getLoginColumn()]);

            if ($user && $user['id'] != $id)
                throw new InvalidArgumentException('Login column value is already in use');
        }

        if (isset($data['password']))
            $data['password'] = Passwords::hash($data['password']);

        return parent::update($id, $data);
    }
    
    /**
     * Finds user by email
     *
     * @param $email
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function findByEmail($email)
    {
        return $this->findAll()
                ->where('email', $email)
                ->fetch();
    }
    
    /**
     * Finds user by login column
     *
     * @param $value
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function findByLoginColumn($value)
    {
        return $this->findAll()
                ->where($this->getLoginColumn(), $value)
                ->fetch();
    }

    /**
     * Finds user by email if he is enabled
     *
     * @param $email
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function findActiveByEmail($email)
    {
        return $this->findByEmail($email)
                ->where('active', TRUE);
    }

    /**
     * Finds users by their role
     *
     * @param $roleId
     * @return \Nette\Database\Table\Selection
     */
    public function findByRoleId($roleId)
    {
        return $this->findAllEnabled()
                ->where('role_id', $roleId);
    }

    /**
     * Finds active users
     *
     * @return \Nette\Database\Table\Selection
     */
    public function findActive()
    {
        return $this->findAll()
                ->where('active', TRUE);
    }

    /**
     * Finds inactive users
     *
     * @return \Nette\Database\Table\Selection
     */
    public function findInactive()
    {
        return $this->findAll()
                ->where('active', FALSE);
    }
    
    

    // PROTECTED METHODS //

    protected function getLoginColumn()
    {
        return 'email';
    }
    
    /**
     * Gets the name of table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'users';
    }
}