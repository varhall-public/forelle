<?php

namespace Varhall\Framework\Model\Tables\Framework\Customers;

/**
 * Description of CustomerResolver
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class CustomerResolver
{
    /// PROPERTIES
    
    /**
     * @var \Nette\Http\IRequest 
     */
    private $_httpRequest = NULL;
    
    /**
     * @var CustomersTable
     */
    private $_customersTable = NULL;

    /**
     * @var string
     */
    private $_adminDomain = NULL;
    
    /**
     * @var \Nette\Database\Table\ActiveRow
     */
    private $_customer = NULL;
    
    
    
    public function __construct(\Nette\Http\IRequest $httpRequest, CustomersTable $service, \Nette\DI\Container $container)
    {
        $this->_httpRequest = $httpRequest;
        $this->_customersTable = $service;

        $this->_adminDomain = isset($container->parameters['adminDomain']) ? $container->parameters['adminDomain'] : FALSE;
    }

    
    /// PUBLIC METHODS
    
    /**
     * Returns object of current customer acording to the domain
     * 
     * @return \Nette\Database\Table\ActiveRow
     */
    public function currentCustomer()
    {
        if ($this->_customer === NULL) {
            $host = $this->_httpRequest->getUrl()->host;
            $domain = explode('.', $host);

            if (!empty($this->_adminDomain) && $domain[0] === $this->_adminDomain)
                $this->_customer = (object) [ 'id' => NULL, 'quotas' => [] ];
            else
                $this->_customer = $this->_customersTable->findByDomain($domain[0]);
        }

        return $this->_customer;
    }
    
    /**
     * Returns ID of current customer acording to the domain
     * 
     * @return int
     */
    public function currentCustomerId()
    {
        return $this->currentCustomer()->id;
    }
}
