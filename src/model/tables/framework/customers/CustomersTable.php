<?php

namespace Varhall\Framework\Model\Tables\Framework\Customers;

/**
 * Description of CustomersTable
 *
 * @author fero
 */
class CustomersTable extends \Varhall\Framework\Model\Tables\Core\AbstractTable
{
    public function __construct(\Nette\Database\Context $database)
    {
        parent::__construct($database);
        
        $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\TimestampPlugin());
        $this->addPlugin(new \Varhall\Framework\Model\Tables\Core\Plugins\JsonPlugin([ 'options', 'quotas' ]));
    }
    
 
    /// PUBLIC METHODS
    
    public function findByDomain($domain)
    {
        $customer = $this->getTable()->where('domain', $domain)->fetch();

        if (!$customer)
            return new NullCustomer();
        
        return $customer;
    }
    
    
    /// PROTECTED METHODS
    
    protected function getTableName()
    {
        return 'customers';
    }
}
