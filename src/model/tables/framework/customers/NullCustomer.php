<?php

namespace Varhall\Framework\Model\Tables\Framework\Customers;

/**
 * Description of NullCustomer
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
class NullCustomer
{
    public function __get($name)
    {
        return NULL;
    }
    
    public function __set($name, $value)
    {
        
    }
    
    public function __call($name, array $arguments)
    {
        return NULL;
    }
}
